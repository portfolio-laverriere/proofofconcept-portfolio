package com.proofOfConceptportfolio.controller;

import com.proofOfConceptportfolio.exception.ProofOfConceptConflictException;
import com.proofOfConceptportfolio.exception.ProofOfConceptNotFoundException;
import com.proofOfConceptportfolio.model.ProofOfConceptEntity;
import com.proofOfConceptportfolio.model.ResourceEntity;
import com.proofOfConceptportfolio.proxy.MicroserviceMediaProxy;
import com.proofOfConceptportfolio.repository.ProofOfConceptRepository;
import com.proofOfConceptportfolio.repository.ResourceRepository;
import com.proofOfConceptportfolio.service.dto.ImageDTO;
import com.proofOfConceptportfolio.service.dto.ProofOfConceptDTO;
import com.proofOfConceptportfolio.service.dto.ResourceDTO;
import com.proofOfConceptportfolio.service.mapper.ProofOfConceptMapper;
import com.proofOfConceptportfolio.service.mapper.ResourceMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataIntegrityViolationException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class ProofOfConceptControllerTest {

    @Mock
    ProofOfConceptRepository proofOfConceptRepository;

    @Mock
    ResourceRepository resourceRepository;

    @Mock
    MicroserviceMediaProxy microserviceMediaProxy;

    @InjectMocks
    ProofOfConceptController proofOfConceptController;

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getAllProofOfConcept() {

        List<ProofOfConceptDTO> proofOfConceptDTOListMock = new ArrayList<>();
        List<ResourceDTO> resourceMockList = new ArrayList<>();
        List<ResourceDTO> resourceMockList2 = new ArrayList<>();
        List<ImageDTO> imageMockList = new ArrayList<>();
        List<ImageDTO> imageMockList2 = new ArrayList<>();

        /* --------------------------------------- MOCK 1 PROOF OF CONCEPT ---------------------------------------- */
        ProofOfConceptDTO proofOfConceptDTOMock1 = ProofOfConceptDTO.builder().id(1).name("Proof of concept test mock 1")
                .publication(new Timestamp(System.currentTimeMillis())).summarize("Résumer pour test").build();

        /* --------------------------------------- MOCK 1 RESOURCE ---------------------------------------- */
        ResourceDTO resourceMock1 = ResourceDTO.builder().id(1).name("Git").url("http://git.com").build();
        resourceMockList.add(resourceMock1);
        proofOfConceptDTOMock1.setResourceDTOList(resourceMockList);

        /* --------------------------------------- MOCK 1 IMAGE ---------------------------------------- */
        ImageDTO imageMock1 = ImageDTO.builder().id(1).name("Proof of concept 1").imagePath("path_image").build();
        imageMockList.add(imageMock1);
        proofOfConceptDTOMock1.setImageDTOList(imageMockList);

        proofOfConceptDTOListMock.add(proofOfConceptDTOMock1);

        /* --------------------------------------- MOCK 2 PROJECT ---------------------------------------- */
        ProofOfConceptDTO proofOfConceptDTOMock2 = ProofOfConceptDTO.builder().id(2).name("Proof of concept  test mock 2")
                .publication(new Timestamp(System.currentTimeMillis())).summarize("Résumer pour test").build();

        /* --------------------------------------- MOCK 2 RESOURCE ---------------------------------------- */
        ResourceDTO resourceMock2 = ResourceDTO.builder().id(2).name("Git").url("http://git.com").build();
        resourceMockList2.add(resourceMock2);
        proofOfConceptDTOMock2.setResourceDTOList(resourceMockList2);

        /* --------------------------------------- MOCK 2 IMAGE ---------------------------------------- */
        ImageDTO imageMock2 = ImageDTO.builder().id(2).name("Proof of concept 2").imagePath("path_image").build();
        imageMockList2.add(imageMock2);
        proofOfConceptDTOMock2.setImageDTOList(imageMockList2);

        proofOfConceptDTOListMock.add(proofOfConceptDTOMock2);

        when(proofOfConceptRepository.findAll())
                .thenReturn(ProofOfConceptMapper.INSTANCE.toEntityList(proofOfConceptDTOListMock));

        when(microserviceMediaProxy.getListImageByProofOfConcept(proofOfConceptDTOMock1.getId()))
                .thenReturn(imageMockList);
        when(resourceRepository.findAllResourceByProofOfConceptId(proofOfConceptDTOMock1.getId()))
                .thenReturn(ResourceMapper.INSTANCE.toEntityList(resourceMockList));

        when(microserviceMediaProxy.getListImageByProofOfConcept(proofOfConceptDTOMock2.getId()))
                .thenReturn(imageMockList2);
        when(resourceRepository.findAllResourceByProofOfConceptId(proofOfConceptDTOMock2.getId()))
                .thenReturn(ResourceMapper.INSTANCE.toEntityList(resourceMockList2));

        final List<ProofOfConceptDTO> proofOfConceptDTOList = proofOfConceptController.getAllProofOfConcept();

        Assertions.assertEquals(proofOfConceptDTOList.size(), 2);
        Assertions.assertEquals(proofOfConceptDTOList.get(0).getResourceDTOList().get(0).getId(), 1);
        Assertions.assertEquals(proofOfConceptDTOList.get(0).getImageDTOList().get(0).getId(), 1);
        Assertions.assertEquals(proofOfConceptDTOList.get(1).getResourceDTOList().get(0).getId(), 2);
        Assertions.assertEquals(proofOfConceptDTOList.get(1).getImageDTOList().get(0).getId(), 2);
    }

    @Test
    void addProofOfConcept() {

        ProofOfConceptEntity proofOfConceptEntityMock = ProofOfConceptEntity.builder().id(8).name("Nom du projet")
                .publication(new Timestamp(System.currentTimeMillis())).summarize("Résumer du projet").build();

        ProofOfConceptDTO proofOfConceptDTOMock = ProofOfConceptDTO.builder().name("Nom du projet")
                .publication(new Timestamp(System.currentTimeMillis())).summarize("Résumer du projet").build();

        when(proofOfConceptRepository.findByName(proofOfConceptDTOMock.getName())).thenReturn(null);
        when(proofOfConceptRepository.save(any())).thenReturn(proofOfConceptEntityMock);

        final ProofOfConceptDTO proofOfConceptDTO = proofOfConceptController.addProofOfConcept(proofOfConceptDTOMock);
        Assertions.assertEquals(proofOfConceptDTO.getId(), proofOfConceptEntityMock.getId());

        when(proofOfConceptRepository.findByName(proofOfConceptDTOMock.getName())).thenReturn(proofOfConceptEntityMock);
        Assertions.assertThrows(ProofOfConceptConflictException.class, () -> {
            proofOfConceptController.addProofOfConcept(proofOfConceptDTOMock);
        });
    }

    @Test
    void addResourceForPOC() {

        List<ResourceDTO> resourceDTOListMock = new ArrayList<>();
        Integer pocIdMock = 3;

        ResourceEntity resourceEntityMock = ResourceEntity.builder().id(2).name("Nom de la ressource")
                .url("URL de la ressource").build();
        ResourceDTO resourceDTOMock = ResourceDTO.builder().name("Nom de la ressource").url("URL de la ressource").build();
        resourceDTOListMock.add(resourceDTOMock);


        when(resourceRepository.findByUrl(resourceDTOMock.getUrl())).thenReturn(resourceEntityMock);
        when(resourceRepository.resourceProofOfConcepttExists(resourceEntityMock.getId(), pocIdMock)).thenReturn(false);
        when(resourceRepository.joinResourceToTheProofOfConcept(resourceEntityMock.getId(), pocIdMock)).thenReturn(1);

        final List<ResourceDTO> resourceDTOList1 = proofOfConceptController.addResourceForPOC(resourceDTOListMock, pocIdMock);
        for (ResourceDTO resourceDTO : resourceDTOList1) {
            Assertions.assertEquals(resourceDTO.getId(), 2);
        }

        when(resourceRepository.findByUrl(resourceDTOMock.getUrl())).thenReturn(null);
        when(resourceRepository.proofOfConceptExists(pocIdMock)).thenReturn(true);
        when(resourceRepository.save(any())).thenReturn(resourceEntityMock);
        when(resourceRepository.joinResourceToTheProofOfConcept(resourceEntityMock.getId(), pocIdMock)).thenReturn(1);

        final List<ResourceDTO> resourceDTOList2 = proofOfConceptController.addResourceForPOC(resourceDTOListMock, pocIdMock);
        for (ResourceDTO resourceDTO : resourceDTOList2) {
            Assertions.assertEquals(resourceDTO.getId(), 2);
        }
    }

    @Test
    void addResourceForPOCException() {

        List<ResourceDTO> resourceDTOListMock = new ArrayList<>();
        Integer pocIdMock  = 5;

        ResourceEntity resourceEntityMock = ResourceEntity.builder().id(1).name("Nom de la ressource")
                .url("URL de la ressource").build();
        ResourceDTO resourceDTOMock = ResourceDTO.builder().name("Nom de la ressource")
                .url("URL de la ressource").build();
        resourceDTOListMock.add(resourceDTOMock);

        when(resourceRepository.findByUrl(resourceDTOMock.getUrl())).thenReturn(resourceEntityMock);
        when(resourceRepository.resourceProofOfConcepttExists(resourceEntityMock.getId(), pocIdMock)).thenReturn(true);
        Assertions.assertThrows(ProofOfConceptConflictException.class, () -> {
            proofOfConceptController.addResourceForPOC(resourceDTOListMock, pocIdMock);
        });

        when(resourceRepository.findByUrl(resourceDTOMock.getUrl())).thenReturn(resourceEntityMock);
        when(resourceRepository.resourceProofOfConcepttExists(resourceEntityMock.getId(), pocIdMock)).thenReturn(false);
        when(resourceRepository.joinResourceToTheProofOfConcept(resourceEntityMock.getId(), pocIdMock))
                .thenThrow(DataIntegrityViolationException.class);
        Assertions.assertThrows(ProofOfConceptNotFoundException.class, () -> {
            proofOfConceptController.addResourceForPOC(resourceDTOListMock, pocIdMock);
        });

        when(resourceRepository.findByUrl(resourceDTOMock.getUrl())).thenReturn(null);
        when(resourceRepository.resourceProofOfConcepttExists(resourceEntityMock.getId(), pocIdMock)).thenReturn(true);
        when(resourceRepository.proofOfConceptExists(pocIdMock)).thenReturn(false);
        Assertions.assertThrows(ProofOfConceptNotFoundException.class, () -> {
            proofOfConceptController.addResourceForPOC(resourceDTOListMock, pocIdMock);
        });
    }

    @Test
    void upProofOfConcept() {

        ProofOfConceptDTO proofOfConceptDTOMock = ProofOfConceptDTO.builder().id(1).name("Proof of concept test mock 1")
                .publication(new Timestamp(System.currentTimeMillis())).summarize("Résumer pour test").build();

        when(proofOfConceptRepository.existsById(proofOfConceptDTOMock.getId())).thenReturn(true);
        when(proofOfConceptRepository.save(any())).thenReturn(ProofOfConceptMapper.INSTANCE.toEntity(proofOfConceptDTOMock));
        final ProofOfConceptDTO proofOfConceptDTO = proofOfConceptController.upProofOfConcept(proofOfConceptDTOMock);
        Assertions.assertEquals(proofOfConceptDTO.getId(), proofOfConceptDTOMock.getId());

        when(proofOfConceptRepository.existsById(proofOfConceptDTOMock.getId())).thenReturn(false);
        Assertions.assertThrows(ProofOfConceptNotFoundException.class, () -> {
            proofOfConceptController.upProofOfConcept(proofOfConceptDTOMock);
        });
    }

    @Test
    void delProofOfConcept() {

        Integer pocIdMock = 1;

        when(proofOfConceptRepository.deleteProofOfConceptById(pocIdMock)).thenReturn(1);
        final String checkStatusSuccess = proofOfConceptController.delProofOfConcept(pocIdMock);
        Assertions.assertEquals(checkStatusSuccess, "Successful deletion of the image");

        when(proofOfConceptRepository.deleteProofOfConceptById(pocIdMock)).thenReturn(0);
        final String checkStatusFailure = proofOfConceptController.delProofOfConcept(pocIdMock);
        Assertions.assertEquals(checkStatusFailure, "Failed deletion - image not found");
    }

    @Test
    void delResourceByProofOfConcept() {

        Integer resourceIdMock = 1;
        Integer pocIdMock = 8;

        when(resourceRepository.deleteResourceByProofOfConcept(resourceIdMock, pocIdMock)).thenReturn(1);
        final String checkStatusSuccess = proofOfConceptController.delResourceByProofOfConcept(resourceIdMock, pocIdMock);
        Assertions.assertEquals(checkStatusSuccess, "Successful deletion of the image");

        when(resourceRepository.deleteResourceByProofOfConcept(resourceIdMock, pocIdMock)).thenReturn(0);
        final String checkStatusFailure = proofOfConceptController.delResourceByProofOfConcept(resourceIdMock, pocIdMock);
        Assertions.assertEquals(checkStatusFailure, "Failed deletion - image not found");
    }
}
