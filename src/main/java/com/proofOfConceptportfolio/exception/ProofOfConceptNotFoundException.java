package com.proofOfConceptportfolio.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ProofOfConceptNotFoundException extends RuntimeException {

    public ProofOfConceptNotFoundException(String s) {
        super(s);
    }
}
