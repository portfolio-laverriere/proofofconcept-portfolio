package com.proofOfConceptportfolio.service.dto;

import lombok.*;

@Getter @Setter @Builder @AllArgsConstructor @NoArgsConstructor
public class ResourceDTO {

    private Integer id;

    private String name;

    private String url;

}
