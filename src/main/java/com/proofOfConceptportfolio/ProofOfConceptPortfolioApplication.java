package com.proofOfConceptportfolio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients("com.proofOfConceptportfolio")
public class ProofOfConceptPortfolioApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProofOfConceptPortfolioApplication.class, args);
	}

}
