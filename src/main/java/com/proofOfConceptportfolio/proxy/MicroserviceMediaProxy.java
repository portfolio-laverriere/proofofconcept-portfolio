package com.proofOfConceptportfolio.proxy;

import com.proofOfConceptportfolio.service.dto.ImageDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import java.util.List;

@FeignClient(name = "MICROSERVICE-MEDIA", url = "http://localhost:9102/MICROSERVICE-MEDIA")
public interface MicroserviceMediaProxy {


    /**
     * OBTENIR TOUTES LES IMAGES PAR L'ID DU PROOF OF CONCEPT
     * @param pocId
     * @return
     */
    @GetMapping(value = "/media/poc/get-all-image/{pocId}")
    List<ImageDTO> getListImageByProofOfConcept(@PathVariable("pocId") Integer pocId);

}
