package com.proofOfConceptportfolio.model;

import lombok.*;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Getter @Setter @Builder @AllArgsConstructor @NoArgsConstructor
@Entity @Table(name = "proof_of_concept")
public class ProofOfConceptEntity {

    @Id @GeneratedValue
    private Integer id;

    private String name;

    private Timestamp publication;

    private String summarize;

}
