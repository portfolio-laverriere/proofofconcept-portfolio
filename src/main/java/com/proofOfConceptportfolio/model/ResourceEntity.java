package com.proofOfConceptportfolio.model;

import lombok.*;
import javax.persistence.*;

@Getter @Setter @Builder @AllArgsConstructor @NoArgsConstructor
@Entity @Table( name = "resource" )
public class ResourceEntity {

    @Id @GeneratedValue
    private Integer id;

    private String name;

    private String url;

}
